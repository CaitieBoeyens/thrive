﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿# :seedling: Thrive 
            
---

## About
Thrive is an app centered around cognitive behavioural therapy (CBT) techniques outlined 
inthe 'Unified Protocol for Transdiagnostic Treatment of Emotional Disorders'. The aim 
is to create a platform for people who are already working with a therapist on CBT.
The app is meant as a kind of anxiety journal where exercises can be completed and 
certain helpful metrics can be tracked.

[Full demo video](https://drive.google.com/file/d/1nHioOUXJlkTOvaNPZWfR5R9C5uvrIbs7/view?usp=sharing)

---

## Features

### Goal Setting
#### Information
Goals are where you start your Thrive journey. It is important to set goals at the 
beginning as it contextualises the rest of the exercises that are completed using 
Thrive. They are all there to get you closer to accomplishing your anxiety goals.

3 goals can be set in Thrive. This is both few enough to keep focused but enough that 
the scope of the goals is not limited. Within each goal there are also 5 steps which need 
to be completed in order to achieve the goal. This allows goals to feel more manageable.

#### Demo

[Video](https://drive.google.com/open?id=1_tjKsWwAl89argzQCaLavMVV_V6MV0St)

---
### OASIS Exercise
#### Information
OASIS in this case stands for Overall Anxiety Severity and Impairment Scale. This exercise
is designed to track anxiety levels week by week. The idea is that when you look at an overview 
of your anxiety in a graph format you can see a general downward trend. There may be ups 
and downs but if you are aware of your anxiety and working on it you should see it decreasing.

The questions are answered on a severity scale. The points on the scale have descriptive values
to choose but they each count a specific value between 0 and 4. When the test is completed, 
a score is generated and plotted onto a graph which gives the history of your anxiety levels. The 
maximum score is 20 and the minimum is 0. The lower the score the lower your anxiety

#### Demo

[Video](https://drive.google.com/file/d/1cTOZ4yyrdKzR4e1aYc-fA9yhs0sIBJYJ/view?usp=sharing)

---
### Worry Time Exercise
#### Information
Worry time is an exercise where you set aside around 15 minutes of your day to do all the worrying
you would normally do throughout the day. During this time worries should just be written down
without filtering them. The worries shouldn't be solved during this time.

In the app you can add your worries by typing them in. The time elapsed is shown on top and after 15
minutes the exercise will end and a summary of the worries will be presented. Extra time can also
be added or the exercise can be ended early. The app will also stop worry time from startingif it is
after 7pm. This is because worry time shouldn't be done too close to the time you go to bed.

#### Demo

[Video](https://drive.google.com/file/d/1TONAcpnriQg4OEpGU1K_aVTr9yN5BO7L/view?usp=sharing)

---
### Coming soon
##### The following features are coming soon
- 4 Quadrant exercise
    - In this exercise a scenario (eg. reducing my anxiety) is evaluated. The pros of changing and staying the same and the cons of changing and staying the same are weighed up. This should show that changing is the way forward.
- Planning and Recording Exposures
    - Exposures are exercises which force you to endure uncomfortable situations in real life (eg. driving alone on the highway). This experience is then used to reappraise the expected outcomes of these situations. 
- Recognising avoidances and safety behaviours
    - Avoidances and safety behaviours are actions that people perform in order to escape the anxiety they feel. It is important to become aware of them because they can be harmful and usually help in the short term but actually cause more long term anxiety.
- Downward arrow technique
    - This is a technique to use when trying to prevent catastrophizing of a situation. "Then what?" will be asked until you cannot come up with anything worse happening or the problem falls apart entirely
- Thinking traps
    - This would be an explanation of the common thinking traps people fall into (eg. catastrophizing) there would also be strategies outlined to deal with them and prevent them. 
- Data sync with therapist 
    - This would unleash the biggest potential of Thrive. It would allow your therapist access to the exercises you had completed so they could review the information before your next session or you could go through it with them in session.

---

Presentation can be found [here](https://docs.google.com/presentation/d/1pZ_T_L6o9hEMSblI-oWJfl6I135Hwa8h0RM8PD8wECI/edit?usp=sharing)

All images in the app and presentation are from [Undraw.co](https://undraw.co/)