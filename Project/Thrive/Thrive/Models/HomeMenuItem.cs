﻿namespace Thrive.Models
{
    public enum MenuItemType
    {
        Home, Worry, Coming, OASIS, Info
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
    }
}
