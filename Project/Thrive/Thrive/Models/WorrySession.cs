﻿using SQLite;
using System;

namespace Thrive.Models
{
    public class WorrySession
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public int Session { get; set; }
        public int NumWorries { get; set; }
        public TimeSpan Duration { get; set; }

    }
}
