﻿using SQLite;
using System;

namespace Thrive.Models
{
    public class OASISEntry
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public int Total { get; set; }
    }
}
