﻿namespace Thrive.Models
{
    class Answer
    {
        public string Rank { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }

    }

}
