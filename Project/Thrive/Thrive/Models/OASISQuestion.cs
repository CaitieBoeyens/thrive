﻿using System.Collections.Generic;

namespace Thrive.Models
{
    class OASISQuestion
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string QuestionNumber { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
