﻿using SQLite;

namespace Thrive.Models
{

    public class Worry
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Content { get; set; }
        public int Session { get; set; }
    }
}
