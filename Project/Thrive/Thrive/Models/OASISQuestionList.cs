﻿using System.Collections.Generic;

namespace Thrive.Models
{
    class OASISQuestionList
    {
        public List<OASISQuestion> Questions { get; set; }
    }
}
