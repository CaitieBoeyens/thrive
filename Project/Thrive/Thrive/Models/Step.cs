﻿using SQLite;
using System;
using System.ComponentModel;

namespace Thrive.Models
{
    public class Step : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int Goal_Num { get; set; }
        public string Action { get; set; }
        public Boolean Completed { get; set; }
        public int Rank { get; set; }
    }
}
