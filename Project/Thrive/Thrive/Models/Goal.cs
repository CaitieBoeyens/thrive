﻿using SQLite;
using System.ComponentModel;

namespace Thrive.Models
{
    public class Goal : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumSteps { get; set; }
        public int NumStepsCompleted { get; set; }
        public int Rank { get; set; }
        public string ImgUrl { get; set; }

    }
}
