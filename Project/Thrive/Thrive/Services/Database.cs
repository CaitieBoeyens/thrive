﻿using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thrive.Models;


namespace Thrive.Services
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);


            _database.CreateTableAsync<Goal>().Wait();
            _database.CreateTableAsync<Step>().Wait();
            _database.CreateTableAsync<OASISEntry>().Wait();
            _database.CreateTableAsync<WorrySession>().Wait();
            _database.CreateTableAsync<Worry>().Wait();
        }

        public Task<List<Goal>> GetGoalsAsync()
        {
            return _database.Table<Goal>().ToListAsync();

        }
        public Task<int> SaveGoalAsync(Goal goal)
        {
            return _database.InsertAsync(goal);
        }

        public Task<int> UpdateGoalAsync(Goal goal)
        {
            return _database.UpdateAsync(goal);
        }

        public Task<List<Step>> GetStepsForGoalAsync(int goalNum)
        {
            return _database.Table<Step>().Where(s => s.Goal_Num.Equals(goalNum)).ToListAsync();
        }

        public Task<List<Step>> GetStepsAsync()
        {
            return _database.Table<Step>().ToListAsync();
        }
        public Task<int> SaveStepAsync(Step step)
        {
            return _database.InsertAsync(step);
        }
        public Task<int> SaveStepsAsync(List<Step> steps)
        {
            return _database.InsertAllAsync(steps);
        }

        public Task<int> UpdateStepAsync(Step step)
        {
            return _database.UpdateAsync(step);
        }

        public Task<int> SaveOASISEntryAsync(OASISEntry entry)
        {
            return _database.InsertAsync(entry);
        }

        public Task<List<OASISEntry>> GetOASISEntrysAsync()
        {
            return _database.Table<OASISEntry>().ToListAsync();
        }

        public Task<int> SaveWorrySessionAsync(WorrySession session)
        {
            return _database.InsertAsync(session);
        }

        public Task<int> SaveWorryAsync(Worry worry)
        {
            return _database.InsertAsync(worry);
        }

        public Task<int> DeleteWorrySessionAsync(WorrySession worrySession)
        {
            return _database.DeleteAsync(worrySession);
        }

        public Task<int> DeleteWorryAsync(Worry worry)
        {
            return _database.DeleteAsync(worry);
        }

        public Task<List<Worry>> GetWorriesForSessionAsync(int session)
        {
            return _database.Table<Worry>().Where(s => s.Session.Equals(session)).ToListAsync();
        }

        public Task<List<WorrySession>> GetWorrySessionsAsync()
        {
            return _database.Table<WorrySession>().ToListAsync();
        }

    }


}

