﻿using System;
using System.ComponentModel;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Thrive.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : MasterDetailPage
    {

        private Boolean firstStart;
        public MainPage()
        {
            InitializeComponent();
            firstStart = Preferences.Get("first_start", true);
            StartGoals();
        }

        private async void StartGoals()
        {
            if (firstStart)
            {
                await Navigation.PushModalAsync(new WelcomePage());
                

            }
        }


    }
}