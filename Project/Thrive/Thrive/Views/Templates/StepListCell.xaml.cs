﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StepListCell : ViewCell
    {
        public StepListCell()
        {
            InitializeComponent();
        }
    }
}