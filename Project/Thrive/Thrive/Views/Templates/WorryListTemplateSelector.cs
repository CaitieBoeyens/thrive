﻿using Thrive.Views.Templates;
using Xamarin.Forms;

namespace Thrive.Templates
{
    class WorryListTemplateSelector : DataTemplateSelector
    {
        DataTemplate worryListDataTemplate;

        public WorryListTemplateSelector()
        {
            this.worryListDataTemplate = new DataTemplate(typeof(WorryListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return worryListDataTemplate;
        }

    }
}