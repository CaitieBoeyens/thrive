﻿using Thrive.Views.Templates;
using Xamarin.Forms;

namespace Thrive.Templates
{
    class StepListTemplateSelector : DataTemplateSelector
    {
        DataTemplate stepListDataTemplate;

        public StepListTemplateSelector()
        {
            this.stepListDataTemplate = new DataTemplate(typeof(StepListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return stepListDataTemplate;
        }

    }
}