﻿using Thrive.Models;
using Thrive.ViewModels;
using Xamarin.Forms;

namespace Thrive.Views.Worry
{

    public partial class WorryDetailPage : ContentPage
    {
        WorryDetailViewModel viewModel;
        WorrySession Session;

        public WorryDetailPage(WorryDetailViewModel viewModel, WorrySession session)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
            Session = session;
            displayNoWorries();
        }

        public void displayNoWorries()
        {
            if (Session.NumWorries == 0)
            {
                worriesList.IsVisible = false;
                noWorries.IsVisible = true;
            }
        }




    }
}