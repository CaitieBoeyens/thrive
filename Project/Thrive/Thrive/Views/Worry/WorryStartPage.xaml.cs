﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Worry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WorryStartPage : ContentPage
    {
        public WorryStartPage()
        {
            InitializeComponent();
            WorryAddingPage.OnDoneWithWorry += GoBackToHome;
        }

        private async void StartWorry_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new WorryAddingPage());
        }

        private async void GoBackToHome()
        {
            WorryAddingPage.OnDoneWithWorry -= GoBackToHome;
            await Navigation.PopModalAsync();
        }
    }
}