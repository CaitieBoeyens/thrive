﻿
using System;
using System.Collections.Generic;
using Thrive.Models;
using Thrive.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Worry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WorryLogPage : ContentPage
    {
        private List<WorrySession> WorrySessions;
        public WorryLogPage()
        {
            InitializeComponent();
            GetEntries();
            DeleteOldSessions();
            MakeList();
        }

        private void GetEntries()
        {
            WorrySessions = App.Database.GetWorrySessionsAsync().Result;
        }

        private void MakeList()
        {
            WorrySessions.Sort((x, y) => y.Date.CompareTo(x.Date));
            sessionList.ItemsSource = WorrySessions;

        }

        private async void DeleteOldSessions()
        {
            for (int i = 0; i < WorrySessions.Count; i++)
            {
                if (DateTime.Now.Subtract(WorrySessions[i].Date).TotalDays >= 7)
                {
                    await App.Database.DeleteWorrySessionAsync(WorrySessions[i]);
                    var worries = App.Database.GetWorriesForSessionAsync(WorrySessions[i].Session).Result;

                    for (int w = 0; w < worries.Count; w++)
                    {
                        await App.Database.DeleteWorryAsync(worries[w]);
                    }
                       
                }
            }
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var session = args.SelectedItem as WorrySession;
            if (session == null)
                return;

            await Navigation.PushAsync(new WorryDetailPage(new WorryDetailViewModel(session), session));

            // Manually deselect item.
            sessionList.SelectedItem = null;
        }
    }
}