﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thrive.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Worry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WorryAddingPage : ContentPage
    {
        private List<string> WorryStrings;
        private int SessionNum;

        private int TotalTime;
        private int TimeElapsed;

        public delegate void DoneWithWorry();
        public static event DoneWithWorry OnDoneWithWorry;
        public WorryAddingPage()
        {
            InitializeComponent();
            WorryStrings = new List<string>();
            TotalTime = 60 * 15;
            TimeElapsed = 0;
            timer.Text = "00:00";
            TimeSpan totalTimeSpan = TimeSpan.FromSeconds(TotalTime);
            totalTime.Text = string.Format("{0:00}:{1:00}", totalTimeSpan.Minutes, totalTimeSpan.Seconds);
            StartTimer();
        }

        private void StartTimer()
        {

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (TimeElapsed <= TotalTime)
                {
                    TimeSpan timeSpan = TimeSpan.FromSeconds(TimeElapsed);
                    timer.Text = string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
                    TimeElapsed++;
                    return true;
                }
                else
                {
                    EndSession();
                    return false;
                }
            });
            TimeSpan totalTimeSpan = TimeSpan.FromSeconds(TotalTime);
            totalTime.Text = string.Format("{0:00}:{1:00}", totalTimeSpan.Minutes, totalTimeSpan.Seconds);
        }

        private void AddWorry_Clicked(object sender, EventArgs e)
        {
            AddWorryToList();
            worryInput.Text = "";
        }

        private void AddWorryToList()
        {
            if (!string.IsNullOrWhiteSpace(worryInput.Text))
            {
                WorryStrings.Add(worryInput.Text);
            }
        }

        private int GetWorrySessionNumber()
        {
            var sessions = App.Database.GetWorrySessionsAsync().Result;
            return sessions.Count();
        }

        private async Task<int> SaveWorries()
        {
            SessionNum = GetWorrySessionNumber() + 1;
            var worryCount = 0;
            for (int i = 0; i < WorryStrings.Count; i++)
            {

                worryCount++;
                await App.Database.SaveWorryAsync(new Models.Worry
                {
                    Content = WorryStrings[i],
                    Session = SessionNum,
                });

            }
            return await App.Database.SaveWorrySessionAsync(new WorrySession
            {
                Session = SessionNum,
                Date = DateTime.Now,
                NumWorries = worryCount,
                Duration = TimeSpan.FromSeconds(TimeElapsed)
            });
        }

        private void DoneWorry_Clicked(object sender, EventArgs e)
        {
            TotalTime = TimeElapsed;
        }

        private void ShowFinishPage()
        {
            var worryItems = App.Database.GetWorriesForSessionAsync(SessionNum).Result;
            worryList.ItemsSource = worryItems;
            worries.IsVisible = false;
            result.IsVisible = true;

            if (worryItems.Count == 0)
            {
                noWorries.IsVisible = true;
                worryList.IsVisible = false;
            }
            else
            {
                noWorries.IsVisible = false;
                worryList.IsVisible = true;
            }
            var duration = TimeSpan.FromSeconds(TimeElapsed).Minutes;
            if (duration < 1)
            {
                duration = TimeSpan.FromSeconds(TimeElapsed).Seconds;
                worryDuration.Text = $"{duration.ToString()} {(duration == 1 ? "second" : "seconds")}";
            } else
            {
                worryDuration.Text = $"{duration.ToString()} {(duration == 1 ? "minute" : "minutes")}";
            }
        }

        private async void WorryFinished_Clicked(object sender, EventArgs e)
        {
            OnDoneWithWorry?.Invoke();
            await Navigation.PopModalAsync();
        }


        private async void EndSession()
        {
            AddWorryToList();
            worryInput.Text = "";
            await SaveWorries();
            ShowFinishPage();
        }

        private void AddMinute_Clicked(object sender, EventArgs e)
        {
            TotalTime += 60;
            TimeSpan totalTimeSpan = TimeSpan.FromSeconds(TotalTime);
            totalTime.Text = string.Format("{0:00}:{1:00}", totalTimeSpan.Minutes, totalTimeSpan.Seconds);
        }
    }
}