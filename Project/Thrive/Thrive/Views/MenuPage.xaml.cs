﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Thrive.Models;
using Thrive.Views.OASIS;
using Thrive.Views.Worry;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Thrive.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        private List<HomeMenuItem> MenuItems;
        public MenuPage()
        {
            InitializeComponent();
            MakeMenuItems();
            menuList.ItemsSource = MenuItems;
        }

        private void MakeMenuItems()
        {
            MenuItems = new List<HomeMenuItem>();
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.OASIS, Title = "OASIS Test", Icon = "\uf201" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Worry, Title = "Worry Time", Icon = "\uf017" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "4 Quadrant", Icon = "\uf009" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "Exposures", Icon = "\uf0ae" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "Explore self", Icon = "\uf002" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "Then what", Icon = "\uf103" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "Thinking traps", Icon = "\uf4ad" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Coming, Title = "Therapist sync", Icon = "\uf2f1" });
            MenuItems.Add(new HomeMenuItem() { Id = MenuItemType.Info, Title = "More Info", Icon = "\uf05a" });
        }

        private void MenuList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;


            NavigateFromMenu(((HomeMenuItem)e.SelectedItem));
        }

        public async void NavigateFromMenu(HomeMenuItem item)
        {
            var id = (int)(item).Id;
            switch (id)
            {
                case (int)MenuItemType.Worry:
                    var now = DateTime.Now.Hour;
                    if (now < 19)
                    {
                        await Navigation.PushModalAsync(new WorryStartPage());
                    }
                    else
                    {
                        await Navigation.PushPopupAsync(new TooLatePopupPage());
                    }

                    break;
                case (int)MenuItemType.Coming:
                    await Navigation.PushPopupAsync(new ComingSoonPopupPage());
                    break;
                case (int)MenuItemType.OASIS:
                    await Navigation.PushModalAsync(new OASISStartPage());
                    break;
                case (int)MenuItemType.Info:
                    await Browser.OpenAsync(new Uri("https://thrive-companion.netlify.com/"), BrowserLaunchMode.SystemPreferred);
                    break;
            }


        }
    }
}