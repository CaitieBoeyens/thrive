﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thrive.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Goals
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GoalSettingPage : ContentPage
    {
        private int goal;
        private int step;
        private int goalStep;

        public delegate void DoneWithGoals();
        public static event DoneWithGoals OnDoneWithGoals;
        public GoalSettingPage()
        {
            InitializeComponent();
            goal = 1;
            step = 1;
            goalStep = 1;
        }

        public GoalSettingPage(int startGoal)
        {
            InitializeComponent();
            goal = startGoal;
            step = 1;
            goalStep = 1;
            ResetNextGoal();
        }

        private void GoalNext_Clicked(object sender, EventArgs e)
        {

            switch (step)
            {
                case 1:
                    if (!string.IsNullOrWhiteSpace(goalTitleInput.Text))
                    {
                        error.Text = "";
                        GoStep2();
                    }
                    else
                    {
                        error.Text = "Please give your goal a title";
                    }
                    break;
                case 2:
                    if (!string.IsNullOrWhiteSpace(goalDescriptionInput.Text))
                    {
                        error.Text = "";
                        GoStep3();
                    }
                    else
                    {
                        error.Text = "Describe your goal to make it more concrete";
                    }
                    break;
                case 3:
                    DoGoalSteps();
                    break;
                default:
                    break;

            }
        }

        private async void GoalDone_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(step1Input.Text))
            {
                await SaveGoal();
                ResetNextGoal();
                error.Text = "";
            }
            else
            {
                error.Text = "Specify a step you would like to complete";
            }
        }

        private async void GoalFinished_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(step5Input.Text))
            {
                await SaveGoal();

                OnDoneWithGoals?.Invoke();

                await Navigation.PopModalAsync();
                error.Text = "";
            }
            else
            {
                error.Text = "Please specify a step you would like to complete";
            }
        }

        private async Task<int> SaveGoal()
        {
            var steps = new List<string>
            { step1Input.Text, step2Input.Text, step3Input.Text, step4Input.Text, step5Input.Text };
            int stepCount = 0;

            for (int i = 0; i < steps.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(steps[i]))
                {
                    stepCount++;
                    await App.Database.SaveStepAsync(new Step
                    {
                        Action = steps[i],
                        Completed = false,
                        Goal_Num = goal,
                        Rank = i + 1
                    });
                }
            }

            return await App.Database.SaveGoalAsync(new Goal
            {
                Title = goalTitleInput.Text,
                Description = goalDescriptionInput.Text,
                NumSteps = stepCount,
                NumStepsCompleted = 0,
                Rank = goal,
                ImgUrl = $"goal{goal}.png"
            });

        }

        private void GoStep2()
        {
            step++;
            goalStepTitle.Text = "Step 2";
            stepNumber.Source = "step_2.png";
            stepDescription.Text = "Now, let’s take a moment to make this goal more concrete. What would it look like once you have achieved this goal? What things would you be doing, or not doing? What behaviors would you be engaging in? What behaviors would you not be engaging in? Try to be as concrete as possible here.";
            goalInputNumber.IsVisible = false;
            goalTitleInput.IsVisible = false;
            goalDescriptionInput.IsVisible = true;
        }
        private void GoStep3()
        {
            step++;
            goalStepTitle.Text = "Step 3";
            stepNumber.Source = "step_3.png";
            stepDescription.Text = "Next, think about some small manageable steps that you can take towards reaching your goal.";
            goalDescriptionInput.IsVisible = false;
            goalSteps.IsVisible = true;
            goalStep1.IsVisible = true;
        }

        private void DoGoalSteps()
        {
            switch (goalStep)
            {
                case 1:
                    if (!string.IsNullOrWhiteSpace(step5Input.Text))
                    {
                        stepDescription.IsVisible = false;
                        goalStep1.IsVisible = false;
                        goalStep2.IsVisible = true;
                        goalStep++;
                        error.Text = "";
                    }
                    else
                    {
                        error.Text = "Please specify a step you would like to complete";
                    }
                    break;
                case 2:
                    if (!string.IsNullOrWhiteSpace(step4Input.Text))
                    {
                        goalStep2.IsVisible = false;
                        goalStep3.IsVisible = true;
                        goalStep++;
                        error.Text = "";
                    }
                    else
                    {
                        error.Text = "Please specify a step you would like to complete";
                    }
                    break;
                case 3:
                    if (!string.IsNullOrWhiteSpace(step3Input.Text))
                    {
                        goalStep3.IsVisible = false;
                        goalStep4.IsVisible = true;
                        goalStep++;
                        error.Text = "";
                    }
                    else
                    {
                        error.Text = "Please specify a step you would like to complete";
                    }
                    break;
                case 4:
                    if (!string.IsNullOrWhiteSpace(step2Input.Text))
                    {
                        goalStep4.IsVisible = false;
                        goalStep5.IsVisible = true;
                        goalNext.IsVisible = false;
                        goalDone.IsVisible = true;
                        goalFinished.IsVisible = true;
                        if (goal == 3)
                        {
                            goalDone.IsVisible = false;
                        }
                        goalStep++;
                        error.Text = "";
                    }
                    else
                    {
                        error.Text = "Please specify a step you would like to complete";
                    }
                    break;
                default:
                    break;
            }

        }

        private void ResetNextGoal()
        {
            ResetValues();
            switch (goal)
            {
                case 1:
                    goalNumber.Text = "Goal 2";
                    goalInputNumber.Text = "My 2nd goal for treatment is:";
                    stepDescription.Text = "People often have at least a few goals for treatment. Let’s take a moment to list at least two more treatment goals you have.";

                    break;
                case 2:
                    goalNumber.Text = "Goal 3";
                    goalInputNumber.Text = "My 3rd goal for treatment is:";
                    stepDescription.Text = "Time to record your third goal";
                    break;
                default:
                    break;
            }

            goalNext.IsVisible = true;
            goalDone.IsVisible = false;
            goalFinished.IsVisible = false;
            stepDescription.IsVisible = true;
            goalInputNumber.IsVisible = true;
            goalTitleInput.IsVisible = true;
            goalSteps.IsVisible = false;
            goalStep5.IsVisible = false;
            goalDescriptionInput.IsVisible = false;
            goal++;
        }

        private void ResetValues()
        {
            step = 1;
            goalStepTitle.Text = "Step 1";
            stepNumber.Source = "step_1.png";
            goalStep = 1;
            goalTitleInput.Text = string.Empty;
            goalDescriptionInput.Text = string.Empty;
            step1Input.Text = string.Empty;
            step2Input.Text = string.Empty;
            step3Input.Text = string.Empty;
            step4Input.Text = string.Empty;
            step5Input.Text = string.Empty;
        }
    }
}