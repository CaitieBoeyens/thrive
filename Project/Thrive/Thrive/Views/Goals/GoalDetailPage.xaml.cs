﻿using Thrive.Models;
using Thrive.ViewModels;
using Xamarin.Forms;

namespace Thrive.Views.Goals
{

    public partial class GoalDetailPage : ContentPage
    {
        GoalDetailViewModel viewModel;
        Goal Goal;

        public GoalDetailPage(GoalDetailViewModel viewModel, Goal goal)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
            Goal = goal;
            displayGoalDone();
        }

        private void StepsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var step = e.Item as Step;
            step.Completed = !step.Completed;
            if (step.Completed)
            {

                Goal.NumStepsCompleted++;
            }
            else
            {

                Goal.NumStepsCompleted--;
            }
            App.Database.UpdateGoalAsync(Goal);
            App.Database.UpdateStepAsync(step);
            displayGoalDone();

        }

        public void displayGoalDone()
        {
            if (Goal.NumSteps == Goal.NumStepsCompleted)
            {
                goalsDone.IsVisible = true;
            }
        }
    }
}