﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.Goals
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GoalStartPage : ContentPage
    {
        public GoalStartPage()
        {
            InitializeComponent();
            GoalSettingPage.OnDoneWithGoals += GoBackToHome;
        }

        private async void StartGoals_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new GoalSettingPage());
        }

        private async void GoBackToHome()
        {
            GoalSettingPage.OnDoneWithGoals -= GoBackToHome;
            await Navigation.PopModalAsync();
        }
    }
}