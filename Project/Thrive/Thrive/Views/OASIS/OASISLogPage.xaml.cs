﻿using Microcharts;
using SkiaSharp;
using System.Collections.Generic;
using Thrive.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.OASIS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OASISLogPage : ContentPage
    {
        private List<OASISEntry> OASISEntries;
        public OASISLogPage()
        {
            InitializeComponent();
            GetEntries();
            CreateChart();
            MakeList();
        }

        private void GetEntries()
        {
            OASISEntries = App.Database.GetOASISEntrysAsync().Result;
        }

        private void MakeList()
        {
            entryList.ItemsSource = OASISEntries;
        }

        private void CreateChart()
        {
            var formatedEntries = new List<Microcharts.Entry>();

            foreach (OASISEntry e in OASISEntries)
            {
                formatedEntries.Add(new Microcharts.Entry(e.Total) { Label = e.Date.ToString("dd MMMM"), Color = SKColor.Parse("#FFFFFF"), ValueLabel = e.Total.ToString(), TextColor = SKColor.Parse("#FFFFFF") });
            }
            var chart = new LineChart() { Entries = formatedEntries.ToArray(), BackgroundColor = SKColor.Parse("#6981F9") };
            this.chartView.Chart = chart;


        }
    }
}