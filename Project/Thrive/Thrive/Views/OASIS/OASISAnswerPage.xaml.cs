﻿using Microcharts;
using Newtonsoft.Json;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Thrive.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.OASIS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OASISAnswerPage : ContentPage
    {
        private int QuestionNumber;
        private OASISQuestionList QuestionList;

        private int[] Answers = { 0, 0, 0, 0, 0 };
        private int Total;

        public delegate void DoneWithOASIS();
        public static event DoneWithOASIS OnDoneWithOASIS;
        public OASISAnswerPage()
        {
            InitializeComponent();
            QuestionNumber = 0;
            populateQuestionsFromJSON();
            question.Text = QuestionList.Questions[QuestionNumber].Description;
            resetQuestion(QuestionNumber);

            OASISDone.IsVisible = false;
        }
        private void populateQuestionsFromJSON()
        {
            string jsonFileName = "Questions.json";
            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.{jsonFileName}");
            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonString = reader.ReadToEnd();

                //Converting JSON Array Objects into generic list    
                QuestionList = JsonConvert.DeserializeObject<OASISQuestionList>(jsonString);
            }
        }

        private void resetQuestion(int disableQuestionNumber)
        {
            UpdateOptionColours("disable", Answers[disableQuestionNumber]);
            UpdateOptionColours("enable", Answers[QuestionNumber]);
            answerTitle.Text = QuestionList.Questions[QuestionNumber].Answers[Answers[QuestionNumber]].Title;
            answerDescription.Text = QuestionList.Questions[QuestionNumber].Answers[Answers[QuestionNumber]].Description;
        }
        private void NextQuestion_Clicked(object sender, EventArgs e)
        {

            QuestionNumber++;
            if (QuestionNumber <= 4)
            {
                if (QuestionNumber == 4)
                {
                    OASISDone.IsVisible = true;
                }
                if (QuestionNumber == 1)
                {
                    previousQuestion.IsEnabled = true;
                }

                topQuestionNumber.Source = $"q{QuestionNumber + 1}.png";
                question.Text = QuestionList.Questions[QuestionNumber].Description;
                resetQuestion(QuestionNumber - 1);
            }
        }

        private void PreviousQuestion_Clicked(object sender, EventArgs e)
        {

            if (QuestionNumber > 0)
            {
                QuestionNumber--;
                if (QuestionNumber == 3)
                {
                    OASISDone.IsVisible = false;
                }
                if (QuestionNumber == 0)
                {
                    previousQuestion.IsEnabled = false;
                }
                topQuestionNumber.Source = $"q{QuestionNumber + 1}.png";
                question.Text = QuestionList.Questions[QuestionNumber].Description;
                resetQuestion(QuestionNumber + 1);
            }
        }

        private void CreateChart()
        {
            var OASISEntries = App.Database.GetOASISEntrysAsync().Result;
            if (OASISEntries.Count == 1)
            {
                chartView.IsVisible = false;
                noEntries.IsVisible = true;
            }
            else
            {

                chartView.IsVisible = true;
                noEntries.IsVisible = false;

                var formatedEntries = new List<Microcharts.Entry>();

                foreach (OASISEntry e in OASISEntries)
                {
                    formatedEntries.Add(new Microcharts.Entry(e.Total) { Label = e.Date.ToString("dd MMMM"), Color = SKColor.Parse("#B178D0"), ValueLabel = e.Total.ToString() });
                }
                var chart = new LineChart() { Entries = formatedEntries.ToArray(), LabelTextSize = 0 };
                this.chartView.Chart = chart;
            }

        }
        private async void DoneOASIS_Clicked(object sender, EventArgs e)
        {
            OnDoneWithOASIS?.Invoke();
            await Navigation.PopModalAsync();

        }

        private void Answer_Tapped(object sender, EventArgs e)
        {
            var answer = sender as Label;
            var value = Int32.Parse(answer.Text);
            var oldValue = Answers[QuestionNumber];
            Answers[QuestionNumber] = value;
            UpdateOptionColours("disable", oldValue);
            UpdateOptionColours("enable", value);

            answerTitle.Text = QuestionList.Questions[QuestionNumber].Answers[value].Title;
            answerDescription.Text = QuestionList.Questions[QuestionNumber].Answers[value].Description;
        }

        private void UpdateOptionColours(string status, int value)
        {
            var colour = "";
            if (status == "disable")
            {
                colour = "#555";
            }
            else
            {
                colour = "#B178D0";
            }


            switch (value)
            {
                case 0:
                    answer0.TextColor = Color.FromHex(colour);
                    break;
                case 1:
                    answer1.TextColor = Color.FromHex(colour);
                    break;
                case 2:
                    answer2.TextColor = Color.FromHex(colour);
                    break;
                case 3:
                    answer3.TextColor = Color.FromHex(colour);
                    break;
                case 4:
                    answer4.TextColor = Color.FromHex(colour);
                    break;
                default:
                    break;
            }
        }

        private async void OASISDone_Clicked(object sender, EventArgs e)
        {
            Total = Answers.Sum();
            questions.IsVisible = false;
            topQuestionNumber.IsVisible = false;
            results.IsVisible = true;
            result.Text = Total.ToString();
            await App.Database.SaveOASISEntryAsync(new OASISEntry { Total = Total, Date = DateTime.Now });
            CreateChart();
        }
    }
}