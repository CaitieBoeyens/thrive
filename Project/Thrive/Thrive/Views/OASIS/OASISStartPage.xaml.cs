﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views.OASIS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OASISStartPage : ContentPage
    {
        public OASISStartPage()
        {
            InitializeComponent();
            OASISAnswerPage.OnDoneWithOASIS += GoBackToHome;
        }

        private async void StartTest_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new OASISAnswerPage());
        }

        private async void GoBackToHome()
        {
            OASISAnswerPage.OnDoneWithOASIS -= GoBackToHome;
            await Navigation.PopModalAsync();
        }
    }
}