﻿using System;
using System.Threading.Tasks;
using Thrive.Views.Goals;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : CarouselPage
    {
        public WelcomePage()
        {
            InitializeComponent();
            GoalSettingPage.OnDoneWithGoals += GoBackToHome;
           
        }

        private async void StartGoals_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new GoalStartPage());
        }

        private async void GoBackToHome()
        {
            Preferences.Set("first_start", false);
            GoalSettingPage.OnDoneWithGoals -= GoBackToHome;
            await Navigation.PopModalAsync();
        }


        private void GoHome_Clicked(object sender, EventArgs e)
        {
            GoBackToHome();
        }

        private void Next_Clicked(object sender, EventArgs e)
        {
            var pageCount = Children.Count;
            if (pageCount < 2)
                return;

            var index = Children.IndexOf(CurrentPage);
            index++;
            if (index >= pageCount)
                index = 0;

            this.CurrentPage = Children[index];
        }

        private void Previous_Clicked(object sender, EventArgs e)
        {
            var pageCount = Children.Count;
            if (pageCount < 2)
                return;

            var index = Children.IndexOf(CurrentPage);
            index--;
            if (index >= pageCount)
                index = 0;

            this.CurrentPage = Children[index];
        }

        private async void LearnMore_Clicked(object sender, EventArgs e)
        {
            await Browser.OpenAsync(new Uri("https://thrive-companion.netlify.com/"), BrowserLaunchMode.SystemPreferred);
        }
    }
}