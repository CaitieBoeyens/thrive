﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Thrive.Models;
using Thrive.ViewModels;
using Thrive.Views.Goals;
using Thrive.Views.OASIS;
using Thrive.Views.Worry;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Thrive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        private ObservableCollection<Goal> Goals;
        private ObservableCollection<OASISEntry> OASISEntries;
        private ObservableCollection<WorrySession> WorrySessions;
        public HomePage()
        {
            InitializeComponent();
            GoalSettingPage.OnDoneWithGoals += GetGoals;
            OASISAnswerPage.OnDoneWithOASIS += ShowOASISScore;
            WorryAddingPage.OnDoneWithWorry += ShowTimeSinceWorry;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetGoals();
            ShowOASISScore();
            ShowTimeSinceWorry();
            
        }

        private void GetGoals()
        {
            Goals = new ObservableCollection<Goal>(App.Database.GetGoalsAsync().Result);

            if (Goals.Count == 0)
            {
                goalListView.IsVisible = false;
                noGoals.IsVisible = true;
                addMore.IsVisible = false;
            }
            else if (Goals.Count < 3)
            {
                goalListView.IsVisible = true;
                addMore.IsVisible = true;
                noGoals.IsVisible = false;
                goalListView.HeightRequest = 125;
            }
            else
            {
                goalListView.IsVisible = true;
                goalListView.HeightRequest = 200;
                addMore.IsVisible = false;
                noGoals.IsVisible = false;
                GoalSettingPage.OnDoneWithGoals -= GetGoals;

            }
            goalListView.ItemsSource = null;
            goalListView.ItemsSource = Goals;
        }

        private void ShowOASISScore()
        {
            OASISEntries = new ObservableCollection<OASISEntry>(App.Database.GetOASISEntrysAsync().Result);

            if (OASISEntries.Count >= 2)
            {

                var difference = GetOASISScoreDifference();
                var points = difference == 1 ? "point" : "points";

                scoreBlock.IsVisible = true;
                noScoreBlock.IsVisible = false;

                scoreDescriptor.Text = "compared to last time";

                if (difference > 0)
                {
                    score.Text = $"{difference.ToString()} {points}";
                    scoreUpArrow.IsVisible = true;
                    scoreDownArrow.IsVisible = false;
                }
                else
                {
                    score.Text = $"{(difference * -1).ToString()} {points}";
                    scoreUpArrow.IsVisible = false;
                    scoreDownArrow.IsVisible = true;
                }
            }
            else if (OASISEntries.Count == 1)
            {
                ShowSingleOASISScore();
            }
            else
            {
                scoreBlock.IsVisible = false;
                noScoreBlock.IsVisible = true;
            }
        }

        private void ShowSingleOASISScore()
        {
            scoreBlock.IsVisible = true;
            noScoreBlock.IsVisible = false;
            var pointsScore = OASISEntries[0].Total;
            var points = pointsScore == 1 ? "point" : "points";
            score.Text = $"{pointsScore.ToString()} {points}";
            scoreUpArrow.IsVisible = false;
            scoreDownArrow.IsVisible = false;
            scoreDescriptor.Text = "last score recorded";
        }

        private int GetOASISScoreDifference()
        {
            var last = OASISEntries[OASISEntries.Count - 1];

            var previous = OASISEntries[OASISEntries.Count - 2];

            return last.Total - previous.Total;
        }

        private void ShowTimeSinceWorry()
        {
            WorrySessions = new ObservableCollection<WorrySession>(App.Database.GetWorrySessionsAsync().Result);
            if (WorrySessions.Count > 0)
            {
                noWorryBlock.IsVisible = false;
                worryBlock.IsVisible = true;
                var time = GetTimeSinceWorry();
                var days = time == 1 ? "day" : "days";
                timeSince.Text = $"{time} {days}";
            }
            else
            {
                noWorryBlock.IsVisible = true;
                worryBlock.IsVisible = false;
            }
        }

        private int GetTimeSinceWorry()
        {
            var lastSession = WorrySessions[WorrySessions.Count - 1];
            return DateTime.Now.Subtract(lastSession.Date).Days;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var goal = args.SelectedItem as Goal;
            if (goal == null)
                return;

            await Navigation.PushAsync(new GoalDetailPage(new GoalDetailViewModel(goal), goal));

            // Manually deselect item.
            goalListView.SelectedItem = null;
        }

        private async void StartGoals_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new GoalStartPage());
        }

        private async void AddGoals_Clicked(object sender, EventArgs e)
        {

            await Navigation.PushModalAsync(new GoalSettingPage(Goals.Count));

        }
        private async void StartOASIS_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new OASISStartPage());
        }

        private async void OASIS_Tapped(object sender, EventArgs e)
        {
            if (OASISEntries.Count > 1)
            {
                await Navigation.PushAsync(new OASISLogPage());
            }
        }

        private async void Worry_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new WorryLogPage());
        }

        private async void StartWorry_Tapped(object sender, EventArgs e)
        {
            var now = DateTime.Now.Hour;
            if (now < 19)
            {
                await Navigation.PushModalAsync(new WorryStartPage());
            }
            else
            {
                await Navigation.PushPopupAsync(new TooLatePopupPage());
            }
        }

        private async void ComingSoon_Tapped(object sender, EventArgs e)
        {
            var label = sender as Label;
            await Navigation.PushPopupAsync(new ComingSoonPopupPage(label.Text));
        }

        // Hide logo on scroll
        private void Main_Scrolled(object sender, ScrolledEventArgs e)
        {
            if (e.ScrollY > 10)
            {
                logo.FadeTo(0, 100, Easing.Linear);
            }
            else
            {
                logo.FadeTo(1, 100, Easing.Linear);

            }

        }
    }
}