﻿using System.Collections.ObjectModel;
using Thrive.Models;

namespace Thrive.ViewModels
{
    public class GoalDetailViewModel : BaseViewModel
    {
        public Goal Goal { get; set; }

        public ObservableCollection<Step> Steps { get; set; }
        public GoalDetailViewModel(Goal goal = null)
        {
            Title = goal?.Description;
            Goal = goal;

            GetSteps();

        }

        private void GetSteps()
        {
            Steps = new ObservableCollection<Step>(App.Database.GetStepsForGoalAsync(Goal.Rank).Result);
        }

    }
}
