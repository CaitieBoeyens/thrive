﻿using System.Collections.ObjectModel;
using Thrive.Models;

namespace Thrive.ViewModels
{
    public class WorryDetailViewModel : BaseViewModel
    {
        public WorrySession Session { get; set; }
        public ObservableCollection<Worry> Worries { get; set; }
        public WorryDetailViewModel(WorrySession session = null)
        {
            Session = session;

            GetWorries();

        }

        private void GetWorries()
        {
            Worries = new ObservableCollection<Worry>(App.Database.GetWorriesForSessionAsync(Session.Session).Result);
        }


    }
}
